Práctica de la materia de PEI, en la que se realizó un Plan Estratégico a un
establecimiento de zapatos llamado "Stylos", localizado en Hermosillo, Sonora.

Algunos temas descritos en el documento son:
- Entrevistas a personas internas y externas a la empresa
- Todo el contexto de la empresa, así como sus fortalezas y debilidades
- Se propuso la Misión y Visión
- Alinear los procesos clave,  objetivos, estratégias, y acciones propuestas
respectivamente.
- Diagrama de Gantt sobre las acciones propuestas

El documento tiene anexadas Tablas, Figuras, y otros documentos necesarios para
llevar la elaboración del Plan Estratégico Informático.